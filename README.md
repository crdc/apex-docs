# Apex

![Build Status](https://gitlab.com/crdc/apex/apex.gitlab.io/badges/master/build.svg)

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install] Gatsby CLI
1. Generate and preview the website with hot-reloading: `gatsby develop`
1. Add content

Read more at Gatsby's [documentation].

[install]: https://www.gatsbyjs.org/docs/
[documentation]: https://www.gatsbyjs.org/docs/

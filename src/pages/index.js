import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'
import Image from '../components/image'

const IndexPage = () => (
  <Layout>
    <h1>[WIP] Apex Documentation</h1>
    <div style={{ maxWidth: '300px', marginBottom: '1.45rem' }}>
      <Image />
    </div>
    <p>
      The future home of documentation for the Apex measurement, control, and
      analysis projects.
    </p>
    <Link to="/about/">About</Link>
  </Layout>
)

export default IndexPage

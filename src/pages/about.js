import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'

const AboutPage = () => (
  <Layout>
    <h1>About Apex</h1>
    <p>Future site of Apex systems documentation and information.</p>
    <Link to="/">Go back</Link>
  </Layout>
)

export default AboutPage

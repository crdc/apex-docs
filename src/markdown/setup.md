# Setup

_This is only an idaelized version of installation for now to work towards_

```sh
img_master='http://dist.coanda.ca/chord/releases/1/images/x86_64/chord-master-1.0.0-x86_64.raw.xz'
img_ui='http://dist.coanda.ca/chord/releases/1/images/x86_64/chord-ui-1.0.0-x86_64.raw.xz'
machinectl pull-raw --verify=no $img_master
tar $img_master
machinectl pull-raw --verify=no $img_ui
tar $img_ui
```

## Testing

### Nspawn Containers

The command used to run the containers with networking bridged needs to be

```sh
systemd-nspawn --network-bridge=br0 -bD /var/lib/machines/my-machine
```

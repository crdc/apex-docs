# PostgreSQL Container

## Setup

This just needs to happen once for all `systemd-nspawn` containers, if status
shows that it's already enabled it's not necessary.

```sh
systemctl enable machines.target
```

Create the container selecting the defaults for packages, but use
`systemd-resolved` for name resolution.

```sh
mkdir /var/lib/machines/postgres
pacstrap -i -c /var/lib/machines/postgres base --ignore linux
# select all, and systemd-resolved
```

Enable the machine.

```sh
systemctl enable systemd-nspawn@postgres
```

### Networking

#### Host Mode

```sh
systemctl enable systemd-nspawn@postgres.service
sed -i 's/network-veth -U//' \
  /etc/systemd/system/machines.target.wants/systemd-nspawn\@postgres.service
```

#### Bridged Mode

_TODO: fix this_

Setup a service file to control the machine. This could probably be done by
editing the `systemd-nspawn@.service` file to add `--network-bridge=br0`, but
that affects all containers and this is for testing.

```sh
systemctl enable systemd-nspawn@postgres.service
sed -i 's/network-veth/network-bridge=br0/' \
  /etc/systemd/system/machines.target.wants/systemd-nspawn\@postgres.service
```

Add the network configuration.

```sh
cat > /tmp/host0.network <<EOF
[Match]
Name=host0

[Network]
DHCP=ipv4
EOF
# FIXME: these need to be tested, ended up doing manually
machinectl copy-to postgres /tmp/host0.network /etc/systemd/network/host0.network
machinectl -M postgres shell
systemctl enable systemd-networkd
systemctl enable systemd-resolved
# press Ctrl+^]]] to exit the container
```

### Configure the Container

Configure the a file system bind for the container to allow it to create the
database.

```sh
mkdir /var/lib/postgres
mkdir -p /etc/systemd/nspawn
cat > /etc/systemd/nspawn/postgres.nspawn <<EOF
[Files]
Bind=/var/lib/postgres
EOF
chown 88.2 /var/lib/postgres
chmod 700 /var/lib/postgres
```

At this point binding the volume showed as `nobody.nobody` unless the container
was started a certain way. The UID and GID settings for `root` were mapped to
something, and it only worked after `chown root.root /bin/su`. This is likely
due to it running as a privileged container, it's just unclear how to resolve
this correctly right now.

Start the machine.

```sh
systemctl start systemd-nspawn@postgres
```

### Database

Install PostgreSQL.

> _ToDo_ Put this into an Ansible role

```sh
machinectl -M postgres shell
pacman -S postgresql
systemctl enable postgresql
# press Ctrl+^]]] to exit the container
```

Create the database and launch the service.

```sh
machinectl -M postgres shell
su - postgres
initdb --locale $LANG -E UTF8 -D '/var/lib/postgres/data'
exit
```

Allow remote connections.

```sh
sed -i "s/^#\(listen_addresses\).*$/\1 = '*'/" /var/lib/postgres/data/postgresql.conf
tee -a /var/lib/postgres/data/pg_hba.conf >/dev/null <<EOF
host    all             all             0.0.0.0/0               md5
host    all             all             ::/0                    md5
EOF
systemctl restart postgresql
```

Setup a password to access to the database.

```sh
su - postgres
psql postgres
\password postgres
# enter a password - b@dPass!
\q
exit
```

That's it, working PostgreSQL container. Test it with `pgcli`.

```sh
pgcli -h 10.10.10.20 -p 5432 -U postgres
```

## ToDo

* [ ] Fix the bind UID/GID issue

# Links to Resources

## Ansible

* https://github.com/jahrik/ansible-arch-workstation

## Rust

* https://rust-lang-nursery.github.io/rust-cookbook/about.html

## GraphQL

* https://github.com/OscarYuen/go-graphql-starter
* https://github.com/tonyghita/graphql-go-example

## Containers

* https://alexmekkering.github.io/Arch-Linux/systemd-nspawn-containers.html
